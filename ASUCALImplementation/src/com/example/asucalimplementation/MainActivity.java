package com.example.asucalimplementation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.text.SimpleDateFormat;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;
import org.opencv.ml.Ml;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener{

	//variables
	String status, photoPath;
	Button process, capture, binarize, original, hsv, hist;
	ImageView image;
	TextView display;
	Intent intent;
	final static int REQUEST_TAKE_PHOTO = 1;
	Bitmap origBitmap, hsvBitmap, binBitmap, maskBitmap;
	Uri imageUri;
	final String TAG = "OpenCV Loader";
	Mat src, orig, hsvMat, binMat, maskMat, hueSatMat;
	
	private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
	     @Override
	     public void onManagerConnected(int status) {
	       switch (status) {
	           case LoaderCallbackInterface.SUCCESS:
	           {
	              Log.i(TAG, "OpenCV loaded successfully");
	              //initialize OpenCV Variables Here
	              src = new Mat();
	              orig = new Mat();
	              hsvMat = new Mat();
	              binMat = new Mat();
	              maskMat = new Mat();
	              hueSatMat = new Mat();
	           } break;


	           default:
	           {
	               super.onManagerConnected(status);
	           } break;
	         }
	      }
	 };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Trying to load OpenCV library");
        if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mOpenCVCallBack))
        {
          Log.e(TAG, "Cannot connect to OpenCV Manager");
        }
        else{ Log.i(TAG, "OpenCV successfull"); 
        System.out.println(java.lang.Runtime.getRuntime().maxMemory()); }
		setContentView(R.layout.activity_main);
		initialize();
			
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void generateHistogram() {
		int row;
		int hue, sat;
		int j=0, k=0;
		float[] data = new float[1];
		
		double[] HS = null, coordinates;
		double temp1, temp2;

		Mat nonZeroCoordinates = new Mat();
		hueSatMat = Mat.zeros(100, 100, CvType.CV_32FC1);
		
		//locate region interest
		Core.findNonZero(binMat, nonZeroCoordinates);
		row = nonZeroCoordinates.rows();
		
		//get h-s values of region of interest 
		//then use h-s values as coordinates for the histogram 
		for(int x=0; x<row; x++){
			coordinates = nonZeroCoordinates.get(x, 0);		//get indices of region of interest
			
			
			//j = row, k = column (j=y, k=x) -> opencv implementation
			j = (int)coordinates[1];
			k = (int)coordinates[0];
			HS = hsvMat.get(j,k);	//coordinates[0,1] gets row and col index		
			
			//make it compatible with MATLAB values
			temp1 = HS[0];
			temp2 = HS[1];
			
			hue = (int)Math.round(temp1/179*100);
			sat = (int)Math.round(temp2/255*100);
			
			hueSatMat.get(hue, sat,data);
			data[0]++;
			hueSatMat.put(hue, sat, data);
		}
		
		hueSatMat = hueSatMat.t();
		
		//reshape 100x100 matrix to 1x10000
		hueSatMat = hueSatMat.reshape(0,1);
		
		display.setText("Histogram Generated");
		
	}
	
	private void predictResult(){
		float result;
		
		CvSVM leafSVM = new CvSVM();
		
		//make sure this directory exists in the phone
		leafSVM.load("/storage/emulated/0/IMAGPRO/model.xml");
		Log.i("Results", "SVM Model loaded");
		
		//will output 1.0 or -1.0
		result = leafSVM.predict(hueSatMat);
		
		if(result == 1.0){
			display.setText("Leaf is less than 7 days old");
		}else{
			display.setText("Leaf is more than 7 days old");
		}
	}
	
	//apply thresholding and binarize image
	private void binarize(){
		binBitmap = Bitmap.createBitmap(origBitmap.getWidth(), origBitmap.getHeight(), origBitmap.getConfig());
		Mat tempMat = new Mat();
		
		//testing RGB 2 GRAY conversion for thresholding
		Imgproc.cvtColor(src, tempMat, Imgproc.COLOR_RGB2GRAY);
		Imgproc.threshold(tempMat, binMat, 110, 240, Imgproc.THRESH_BINARY_INV);
		
		Utils.matToBitmap(binMat, binBitmap);
		image.setImageBitmap(binBitmap);
	}
	
	//convert to hsv
	private void hsvConvert(){
		Log.i("HSV Convert", "Executing statements");
		hsvBitmap = Bitmap.createBitmap(origBitmap.getWidth(), origBitmap.getHeight(), origBitmap.getConfig());
		
		Imgproc.cvtColor(src, hsvMat, Imgproc.COLOR_RGB2HSV);
		
		Utils.matToBitmap(hsvMat, hsvBitmap);
		
		
		display.setText("HSV Conversion Done");
		image.setImageBitmap(hsvBitmap);
	}
	
	
	
	private void initialize(){
		status = "No image to process";
		
		//initialize buttons
		original = (Button) findViewById(R.id.origBtn);
		process = (Button) findViewById(R.id.processBtn);
		capture = (Button) findViewById(R.id.captureBtn);
		binarize = (Button) findViewById(R.id.binarizeBtn);		
		hsv = (Button) findViewById(R.id.hsvBtn);
		hist = (Button) findViewById(R.id.histBtn);
		
		//initialize views
		display = (TextView) findViewById(R.id.statusTV);
		image = (ImageView) findViewById(R.id.pictureIV);
		
		process.setOnClickListener(this);
		capture.setOnClickListener(this);
		binarize.setOnClickListener(this);
		original.setOnClickListener(this);
		hsv.setOnClickListener(this);
		hist.setOnClickListener(this);
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.captureBtn:
			capturePhoto();
			
			break;
		case R.id.origBtn:
			if(origBitmap != null){
				image.setImageBitmap(origBitmap);			
			}else{
				display.setText(status);
			}

			break;
		case R.id.binarizeBtn:
			if(origBitmap != null){
				binarize();	
			}else{
				display.setText(status);
			}

			break;
		case R.id.histBtn:
			if(origBitmap != null && binBitmap != null && hsvBitmap != null){
				display.setText("Generating Histogram. Please wait.");
				generateHistogram();	
			}else{
				display.setText(status);
			}
			break;
		case R.id.hsvBtn:
			
			if(origBitmap != null){
				hsvConvert();
			}else{
				display.setText(status);
			}
			
			break;
		case R.id.processBtn:
			if(origBitmap !=null && hsvBitmap != null && hueSatMat != null){
				predictResult();
			}else{
				display.setText(status);
			}
		}
	}
	
	
	//capture photos with this method
	private void capturePhoto(){
		intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		
		if(intent.resolveActivity(getPackageManager()) != null){
			
			File photoFile = null;
			
			try {
	            photoFile = createImageFile();
	        } catch (IOException ex) {
	            // Error occurred while creating the File
	           
	        }
	        // Continue only if the File was successfully created
			if(photoFile != null){
				imageUri = Uri.fromFile(photoFile);
			
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				startActivityForResult(intent, REQUEST_TAKE_PHOTO);				
			}else{
				startActivityForResult(intent, REQUEST_TAKE_PHOTO);
			}
		}
	}
		
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK && requestCode == REQUEST_TAKE_PHOTO){
			try{
				//initially used the following for acquiring images
//				getContentResolver().notifyChange(imageUri, null);
//				ContentResolver cr = this.getContentResolver();
//				origBitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, imageUri);
//				Log.i("On Activity Result", "HD Bitmap Created");
//				setPic();
//				hdBitmap.recycle();
				
				//read image and convert from bgr to rgb
				orig = Highgui.imread(photoPath);
				Imgproc.cvtColor(orig, orig, Imgproc.COLOR_BGR2RGB);
				
				//resample/resize image by a factor of 5
				Size size = new Size(orig.cols()/5.0,orig.rows()/5.0);
				Imgproc.resize(orig, src, size);
				
				//initialize bitmap object
				origBitmap = Bitmap.createBitmap(src.width(), src.height(), Bitmap.Config.ARGB_8888);		
				
				//convert matrix to bitmap
				Utils.matToBitmap(src, origBitmap);
				image.setImageBitmap(origBitmap);
				display.setText("Image Captured!");
				
//				initially used when getting thumbnail sized photos
//				image.setImageBitmap(origBitmap);
//				Bundle extras = data.getExtras();
//				origBitmap = (Bitmap) extras.get("data");
//				Utils.bitmapToMat(origBitmap, src);
//				image.setImageBitmap(origBitmap);
			}catch(Exception e){
				e.printStackTrace();
			}

				
		}
	}
	
	
	//create image file for high def pictures
	private File createImageFile() throws IOException {
	    // Create an image file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    String imageFileName = "JPEG_" + timeStamp + "_";
	    File storageDir = Environment.getExternalStoragePublicDirectory(
	            Environment.DIRECTORY_PICTURES);
	    File imageFile = File.createTempFile(
	        imageFileName,  // prefix 
	        ".jpg",         // suffix 
	        storageDir      // directory 
	    );

	    // Save a file: path for use with ACTION_VIEW intents
	    photoPath = imageFile.getAbsolutePath();
	    return imageFile;
	}
	
	/* previously used to resize picture
	private void setPic(){
		int targetW = image.getWidth();
		int targetH = image.getHeight();
		
		// Get the dimensions of the bitmap
	    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    bmOptions.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(photoPath, bmOptions);
	    int photoW = bmOptions.outWidth;
	    int photoH = bmOptions.outHeight;

	    // Determine how much to scale down the image
	    int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

	    // Decode the image file into a Bitmap sized to fill the View
	    bmOptions.inJustDecodeBounds = false;
	    bmOptions.inSampleSize = 5;
	    bmOptions.inPurgeable = true;

	    origBitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
	    image.setImageBitmap(origBitmap);
	}
	*/
	
}
